#!/bin/bash
work_dir=$(pwd)
mkdir -p ~/.config
mkdir -p ~/.config/fish
sudo echo ParallelDownloads = 5 | sudo tee -a /etc/pacman.conf

gpu_is_nvidia=n

while getopts ":n" arg; do
    case "${arg}" in
        n) gpu_is_nvidia=y
    esac
done

sudo pacman -Syu
sudo pacman -S --needed - < ./my-pkgs.txt

git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..

yay -S lingot

if [ "$gpu_is_nvidia" = "y" ]
then
    sudo pacman -S nvidia nvidia-lts
else
    sudo pacman -S mesa
fi

cd ~/.config
git clone https://gitlab.com/c6936/st.git
cd st
sudo make install
cd $work_dir

cd ~
git clone https://gitlab.com/c6936/dwm.git
mv dwm .dwm
cd .dwm/dwm-6.4/
sudo make install
cd $work_dir
sudo mkdir -p /usr/share/xsessions
sudo cp ~/.dwm/dwm.desktop /usr/share/xsessions/

git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install
~/.emacs.d/bin/doom sync

git clone https://gitlab.com/c6936/shellrc.git
cd shellrc/
/bin/bash shellrc.sh -l exa
cd ..

echo -e "Defaults\tenv_reset,pwfeedback" | sudo tee -a /etc/sudoers

sudo systemctl enable lightdm
sudo systemctl enable sshd
sudo systemctl enable syncthing@jon
echo "reboot this bitch"
